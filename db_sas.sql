-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 04, 2017 at 04:48 AM
-- Server version: 5.7.14
-- PHP Version: 5.6.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_sas`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_attendance`
--

CREATE TABLE `tbl_attendance` (
  `id` int(11) NOT NULL,
  `roll` int(11) NOT NULL,
  `attend` varchar(255) DEFAULT NULL,
  `att_time` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_attendance`
--

INSERT INTO `tbl_attendance` (`id`, `roll`, `attend`, `att_time`) VALUES
(1, 11, 'present', '2017-04-02'),
(2, 12, 'present', '2017-04-02'),
(3, 13, 'present', '2017-04-02'),
(4, 14, 'present', '2017-04-02'),
(5, 15, 'present', '2017-04-02'),
(8, 11, 'absent', '2017-04-01'),
(9, 12, 'absent', '2017-04-01'),
(10, 13, 'absent', '2017-04-01'),
(11, 14, 'absent', '2017-04-01'),
(12, 15, 'absent', '2017-04-01'),
(13, 16, 'absent', '2017-04-01'),
(14, 17, 'absent', '2017-04-01'),
(15, 18, 'absent', '2017-04-01'),
(16, 19, 'absent', '2017-04-01'),
(48, 19, 'present', '2017-04-03'),
(47, 18, 'absent', '2017-04-03'),
(46, 17, 'present', '2017-04-03'),
(45, 16, 'absent', '2017-04-03'),
(44, 15, 'present', '2017-04-03'),
(43, 14, 'absent', '2017-04-03'),
(42, 13, 'present', '2017-04-03'),
(41, 12, 'absent', '2017-04-03'),
(40, 11, 'present', '2017-04-03'),
(49, 11, 'absent', '2017-04-04'),
(50, 12, 'present', '2017-04-04'),
(51, 13, 'absent', '2017-04-04'),
(52, 14, 'present', '2017-04-04'),
(53, 15, 'absent', '2017-04-04'),
(54, 16, 'present', '2017-04-04'),
(55, 17, 'absent', '2017-04-04'),
(56, 18, 'present', '2017-04-04'),
(57, 19, 'absent', '2017-04-04');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_student`
--

CREATE TABLE `tbl_student` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `roll` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_student`
--

INSERT INTO `tbl_student` (`id`, `name`, `roll`) VALUES
(1, 'Test', 11),
(2, 'S Ahmed Naim', 12),
(3, 'Hariz Mohammad', 13),
(4, 'Nurul Huda Sarker', 14),
(5, 'Alamgir Hossain', 15),
(6, 'Limon Sarker', 16),
(7, 'Shakil Ahmed', 17),
(8, 'Nobin Khan', 18),
(9, 'Naim Sarker', 19);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_attendance`
--
ALTER TABLE `tbl_attendance`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_student`
--
ALTER TABLE `tbl_student`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_attendance`
--
ALTER TABLE `tbl_attendance`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=58;
--
-- AUTO_INCREMENT for table `tbl_student`
--
ALTER TABLE `tbl_student`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
